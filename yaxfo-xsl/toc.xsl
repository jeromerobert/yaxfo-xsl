<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Customized from http://tecfa.unige.ch/guides/xml/examples/xsl-toc/xhtml-toc-ns-numbered.xsl -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns="http://www.w3.org/1999/xhtml">
  <xsl:param name="toc-depth" select="3"/>

  <xsl:template match="h:body|body" mode="toc">
    <xsl:apply-templates mode="toc" select="//h:h1|//h:h2|//h:h3|//h:h4|//h:h5|//h:h6"/>
  </xsl:template>

  <xsl:template match="h:h1" mode="section_id">
    <xsl:number level="any" from="h:body" count="h:h1"/>
  </xsl:template>

  <xsl:template match="h:h2" mode="section_id">
    <xsl:number level="any" from="h:body" count="h:h1"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h1" count="h:h2"/>
  </xsl:template>

  <xsl:template match="h:h3" mode="section_id">
    <xsl:number level="any" from="h:body" count="h:h1"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h1" count="h:h2"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h2" count="h:h3"/>
  </xsl:template>

  <xsl:template match="h:h4" mode="section_id">
    <xsl:number level="any" from="h:body" count="h:h1"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h1" count="h:h2"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h2" count="h:h3"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h3" count="h:h4"/>
  </xsl:template>

  <xsl:template match="h:h5" mode="section_id">
    <xsl:number level="any" from="h:body" count="h:h1"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h1" count="h:h2"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h2" count="h:h3"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h3" count="h:h4"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h4" count="h:h5"/>
  </xsl:template>

  <xsl:template match="h:h6" mode="section_id">
    <xsl:number level="any" from="h:body" count="h:h1"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h1" count="h:h2"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h2" count="h:h3"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h3" count="h:h4"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h4" count="h:h5"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h5" count="h:h6"/>
  </xsl:template>
 
  <xsl:template match="h:h1" mode="toc">
    <xsl:if test="$toc-depth &gt; 0">
      <fo:block text-align-last="justify" font-weight="bold" margin-top="1em">
        <fo:basic-link internal-destination="{generate-id(.)}">
          <xsl:apply-templates mode="section_id" select="."/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
          <fo:leader leader-pattern="dots"/>
          <fo:page-number-citation ref-id="{generate-id(.)}"/>
        </fo:basic-link>
      </fo:block>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h2" mode="toc">
    <xsl:if test="$toc-depth &gt; 1">
      <fo:block text-align-last="justify" margin-left="1em">
        <fo:basic-link internal-destination="{generate-id(.)}">
          <xsl:apply-templates mode="section_id" select="."/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
          <fo:leader leader-pattern="dots"/>
          <fo:page-number-citation ref-id="{generate-id(.)}"/>
        </fo:basic-link>
      </fo:block>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h3" mode="toc">
    <xsl:if test="$toc-depth &gt; 2">
      <fo:block text-align-last="justify" margin-left="2em">
        <fo:basic-link internal-destination="{generate-id(.)}">
          <xsl:apply-templates mode="section_id" select="."/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
          <fo:leader leader-pattern="dots"/>
          <fo:page-number-citation ref-id="{generate-id(.)}"/>
        </fo:basic-link>
      </fo:block>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h4" mode="toc">
    <xsl:if test="$toc-depth &gt; 3">
      <fo:block text-align-last="justify" margin-left="3em">
        <fo:basic-link internal-destination="{generate-id(.)}">
          <xsl:apply-templates mode="section_id" select="."/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
          <fo:leader leader-pattern="dots"/>
          <fo:page-number-citation ref-id="{generate-id(.)}"/>
        </fo:basic-link>
      </fo:block>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h5" mode="toc">
    <xsl:if test="$toc-depth &gt; 4">
      <fo:block text-align-last="justify" margin-left="4em">
        <fo:basic-link internal-destination="{generate-id(.)}">
          <xsl:apply-templates mode="section_id" select="."/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
          <fo:leader leader-pattern="dots"/>
          <fo:page-number-citation ref-id="{generate-id(.)}"/>
        </fo:basic-link>
      </fo:block>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h6" mode="toc">
    <xsl:if test="$toc-depth &gt; 5">
      <fo:block text-align-last="justify" margin-left="5em">
        <fo:basic-link internal-destination="{generate-id(.)}">
          <xsl:apply-templates mode="section_id" select="."/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
          <fo:leader leader-pattern="dots"/>
          <fo:page-number-citation ref-id="{generate-id(.)}"/>
        </fo:basic-link>
      </fo:block>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h1">
    <fo:block break-before="page"/>
    <fo:block id="{generate-id(.)}" font-size="180%" font-weight="bold" margin-top="1em" margin-bottom="0.5em">
      <xsl:call-template name="common-atts"/>
      <xsl:apply-templates mode="section_id" select="."/>
      <xsl:text>. </xsl:text>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="h:h2">
    <fo:block id="{generate-id(.)}" font-size="160%" font-weight="bold" keep-with-next="always" margin-top="0.5em">
      <xsl:call-template name="common-atts"/>
      <xsl:apply-templates mode="section_id" select="."/>
      <xsl:text>. </xsl:text>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="h:h3">
    <fo:block id="{generate-id(.)}" font-size="140%" font-weight="bold" margin-top="0.5em" keep-with-next="always">
      <xsl:call-template name="common-atts"/>
      <xsl:apply-templates mode="section_id" select="."/>
      <xsl:text>. </xsl:text>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
 
  <xsl:template match="h:h4">
    <fo:block id="{generate-id(.)}" font-size="120%" font-weight="bold" keep-with-next="always">
      <xsl:call-template name="common-atts"/>
      <xsl:apply-templates mode="section_id" select="."/>
      <xsl:text>. </xsl:text>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
 
  <xsl:template match="h:h5">
    <fo:block id="{generate-id(.)}" font-size="100%" font-weight="bold">
      <xsl:call-template name="common-atts"/>
      <xsl:apply-templates mode="section_id" select="."/>
      <xsl:text>. </xsl:text>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
 
  <xsl:template match="h:h6">
    <fo:block id="{generate-id(.)}" font-size="100%" font-weight="bold">
      <xsl:call-template name="common-atts"/>
      <xsl:apply-templates mode="section_id" select="."/>
      <xsl:text>. </xsl:text>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
 
</xsl:stylesheet>
