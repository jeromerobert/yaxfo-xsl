<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns:h="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="svg"
                version='1.0'>
  
  <xsl:template match="svg:svg" xmlns:svg="http://www.w3.org/2000/svg">
    <fo:instream-foreign-object>
      <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates/>
      </xsl:copy>
    </fo:instream-foreign-object>
  </xsl:template>
  
  <xsl:template match="svg:*" xmlns:svg="http://www.w3.org/2000/svg">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="h:object[starts-with(@type,'image/')]|object[starts-with(@type,'image/')]">
    <xsl:choose>
      <xsl:when test="string-length(normalize-space(../text()))=0 and count(../*)=1">
        <fo:block text-align="center">
          <xsl:call-template name="svg.object"/>
        </fo:block>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="svg.object"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="svg.object">
    <fo:external-graphic content-type="{@type}" src="{concat(//base/@href,@data)}">
      <xsl:if test="@width">
        <xsl:attribute name="content-width">
          <xsl:value-of select="@width"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="@height">
        <xsl:attribute name="content-height">
          <xsl:value-of select="@height"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:call-template name="common-atts"/>
    </fo:external-graphic>
  </xsl:template>

  <!-- center images -->
  <xsl:template match="h:img|img|h:input[@type='image']|input[@type='image']">
    <xsl:choose>
      <xsl:when test="string-length(normalize-space(../text()))=0 and count(../*)=1">
        <fo:block text-align="center">
          <fo:external-graphic content-type="{@type}" src="{concat(//base/@href,@src)}" content-width="scale-to-fit">
            <xsl:call-template name="common-atts"/>
          </fo:external-graphic>
        </fo:block>
      </xsl:when>
      <xsl:otherwise>
        <fo:external-graphic content-type="{@type}" src="{concat(//base/@href,@src)}" content-width="scale-to-fit">
          <xsl:call-template name="common-atts"/>
        </fo:external-graphic>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  
</xsl:stylesheet>
