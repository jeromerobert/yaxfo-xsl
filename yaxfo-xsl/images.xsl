<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xsl:template match="xhtml:img|img|xhtml:input[@type='image']|input[@type='image']">
  <fo:external-graphic content-type="{@type}" src="{concat(//base/@href,@src)}" content-height="scale-to-fit" content-width="scale-to-fit" >
    <xsl:call-template name="common-atts"/>
  </fo:external-graphic>
</xsl:template>

<xsl:template match="xhtml:object[starts-with(@type,'image/')]|object[starts-with(@type,'image/')]">
  <fo:external-graphic content-type="{@type}" src="{concat(//base/@href,@data)}" content-height="scale-to-fit" content-width="scale-to-fit">
    <xsl:call-template name="common-atts"/>
  </fo:external-graphic>
</xsl:template>
</xsl:stylesheet>

