<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                exclude-result-prefixes="mml"
                version='1.0'>
  
  <xsl:template match="mml:math" xmlns:mml="http://www.w3.org/1998/Math/MathML">
    <fo:instream-foreign-object>
      <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates/>
      </xsl:copy>
    </fo:instream-foreign-object>
  </xsl:template>
  
  <xsl:template match="mml:*" xmlns:mml="http://www.w3.org/1998/Math/MathML">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
