<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xhtml="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="xhtml">

  <xsl:template match="xhtml:table">
    <xsl:param name="hooked"/>
    <xsl:apply-templates select="caption"/>
    <fo:table width="100%" table-layout="fixed" keep-together.within-column="always" margin-top="1.5em" margin-bottom="1.5em"><xsl:call-template name="common-atts"/>
      <xsl:apply-templates select="colgroup|col"/>
      <xsl:variable name="tr1" select="(xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:tfoot/xhtml:tr)[1]"/>
      <xsl:variable name="cols" select="xhtml:colgroup/xhtml:col|xhtml:col"/>
      <xsl:call-template name="mock-col">
        <xsl:with-param name="cols" select="(count($tr1/xhtml:*[not(@colspan)])+sum($tr1/xhtml:*/@colspan))
        -(count($cols[not(@colspan)])+sum($cols/@colspan))"/>
      </xsl:call-template>
      <xsl:apply-templates select="xhtml:thead|xhtml:tfoot|xhtml:tbody"/>
      <xsl:if test="xhtml:tr">
        <fo:table-body>
          <xsl:call-template name="common-atts"/>
          <xsl:apply-templates select="xhtml:tr"/>
        </fo:table-body>
      </xsl:if>
    </fo:table>
  </xsl:template>
 
  <xsl:template match="table">
    <xsl:apply-templates select="caption"/>
    <fo:table keep-together="always" width="100%"><xsl:call-template name="common-atts"/>
      <xsl:apply-templates select="colgroup|col"/>
      <xsl:variable name="tr1" select="(tr|thead/tr|tbody/tr|tfoot/tr)[1]"/>
      <xsl:variable name="cols" select="colgroup/col|col"/>
      <xsl:call-template name="mock-col">
        <xsl:with-param name="cols" select="(count($tr1/*[not(@colspan)])+sum($tr1/*/@colspan))
        -(count($cols[not(@colspan)])+sum($cols/@colspan))"/>
      </xsl:call-template>
      <xsl:apply-templates select="thead|tfoot|tbody"/>
      <xsl:if test="tr">
        <fo:table-body><xsl:call-template name="common-atts"/>
          <xsl:apply-templates select="tr"/>
        </fo:table-body>
      </xsl:if>
    </fo:table>
  </xsl:template>
 
  <xsl:template match="xhtml:th|th">
    <fo:table-cell font-weight="bold" padding=".1em" border-style="solid" border-width="0.2mm" text-align="center" background-color="#CCBB22">
      <xsl:call-template name="common-atts"/>
      <xsl:if test="@colspan">
        <xsl:attribute name="number-columns-spanned">
          <xsl:value-of select="@colspan"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="@rowspan">
        <xsl:attribute name="number-rows-spanned">
          <xsl:value-of select="@rowspan"/>
        </xsl:attribute>
      </xsl:if>
      <fo:block>
        <xsl:if test="parent::xhtml:tr/parent::xhtml:thead|parent::tr/parent::thead">
          <xsl:attribute name="text-align">center</xsl:attribute>
        </xsl:if>
        <xsl:apply-templates/>
      </fo:block>
    </fo:table-cell>
  </xsl:template>
 
  <xsl:template match="xhtml:td|td">
    <fo:table-cell padding=".1em" border-width="0.2mm">
      <xsl:if test="not(ancestor::xhtml:table[1]/@border) or ancestor::xhtml:table[1]/@border!='0'">
        <xsl:attribute name="border-style">solid</xsl:attribute>
      </xsl:if>
      <xsl:call-template name="common-atts"/>
      <xsl:if test="@colspan">
        <xsl:attribute name="number-columns-spanned">
          <xsl:value-of select="@colspan"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="@rowspan">
        <xsl:attribute name="number-rows-spanned">
          <xsl:value-of select="@rowspan"/>
        </xsl:attribute>
      </xsl:if>
      <fo:block>
        <xsl:if test="@visibility">
          <xsl:attribute name="visibility">
            <xsl:value-of select="@visibility"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:apply-templates/>
      </fo:block>
    </fo:table-cell>
  </xsl:template>
 
</xsl:stylesheet>
