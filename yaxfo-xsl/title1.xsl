<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:h="http://www.w3.org/1999/xhtml">

  <xsl:template name="title-page">
    <fo:table table-layout="fixed" width="100%">
      <fo:table-column column-width="proportional-column-width(1)"/>
      <fo:table-body>
        <fo:table-row height="257mm">
          <fo:table-cell display-align="center">
            <fo:block text-align="center" font-size="220%" font-weight="bold">
              <xsl:value-of select="//h:title"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>
</xsl:stylesheet>

