<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:h="http://www.w3.org/1999/xhtml" exclude-result-prefixes="h"
  xmlns:str="http://exslt.org/strings" xmlns:math="http://exslt.org/math"
  xmlns:common="http://exslt.org/common"
  xmlns:java="http://xml.apache.org/xslt/java"
  extension-element-prefixes="str math common java">

  <xsl:include href="xhtml2fo.xsl"/>
  <xsl:include href="images.xsl"/>
  <xsl:include href="table.xsl"/>
  <xsl:include href="svg.xsl"/>
  <xsl:include href="jlatexmath.xsl"/>
  <xsl:include href="mathml.xsl"/>
  <xsl:include href="slides-toc.xsl"/>

  <xsl:param name="font-size" select="16"/>
  <xsl:param name="font.symbol" select="'any'"/>
  <xsl:param name="logo" select="'logo.png'"/>

  <xsl:template match="h:h1">
    <fo:page-sequence master-reference="page">
      <xsl:call-template name="footer"/>
      <fo:flow flow-name="xsl-region-body">
        <fo:table id="{generate-id(.)}" table-layout="fixed" width="100%">
          <fo:table-column column-width="proportional-column-width(1)"/>
          <fo:table-body>
            <fo:table-row height="13.55cm">
              <fo:table-cell display-align="center">
                <fo:block text-align="center" color="rgb(0,52,120)" font-size="200%" font-weight="bold">
                  <xsl:apply-templates/>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
      </fo:flow>
    </fo:page-sequence>
  </xsl:template>

  <xsl:template match="h:html|html">
    <fo:root font-family="DejaVu Sans Condensed">
      <xsl:if test="$font-size">
        <xsl:attribute name="font-size">
          <xsl:value-of select="$font-size"/>
        </xsl:attribute>
      </xsl:if>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="page" page-width="25.4cm" page-height="19.05cm">
          <fo:region-body margin="3.5cm 1cm 2cm 1cm"/>
          <fo:region-before extent="3.5cm"/>
          <fo:region-after extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="page">
        <xsl:call-template name="footer">
          <xsl:with-param name="slide-number" select="0"/>
        </xsl:call-template>
        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates select="h:head/h:title|head/title"/>
        </fo:flow>
      </fo:page-sequence>
      <fo:page-sequence master-reference="page">
        <fo:static-content flow-name="xsl-region-before">
          <fo:block id="{generate-id(.)}" text-align="center" color="rgb(0,52,120)" font-size="160%" font-weight="bold" margin="2em 0 0 2em">
            Agenda
          </fo:block>
        </fo:static-content>
        <xsl:call-template name="footer">
          <xsl:with-param name="slide-number" select="1"/>
        </xsl:call-template>
        <fo:flow flow-name="xsl-region-body">
        <fo:table table-layout="fixed" width="100%">
          <fo:table-column column-width="proportional-column-width(1)"/>
            <fo:table-body>
              <fo:table-row height="13.55cm">
                <fo:table-cell display-align="center">
                  <xsl:apply-templates select="h:body" mode="toc"/>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:flow>
      </fo:page-sequence>
      <xsl:apply-templates/>
    </fo:root>
  </xsl:template>

  <xsl:template name="footer">
    <xsl:param name="slide-number" select="count(preceding-sibling::h:h2|preceding-sibling::h:h1)+2"/>
    <fo:static-content flow-name="xsl-region-after">
      <fo:block display-align="after" text-align-last="justify" margin-left="1cm" margin-right="1cm" color="rgb(0,152,219)">
        <xsl:if test="$slide-number &gt; 0">
          <xsl:value-of select="$slide-number"/>
          <xsl:text> / </xsl:text>
          <xsl:value-of select="count(//h:h2|//h:h1)+1"/>
        </xsl:if>
        <fo:leader/>
        <fo:external-graphic src="{$logo}" content-height="2em"/>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="header">
    <fo:static-content flow-name="xsl-region-before">
       <fo:block id="{generate-id(.)}" color="rgb(0,52,120)" font-size="160%" font-weight="bold" margin="2em 0 0 1em">
         <xsl:call-template name="common-atts"/>
         <xsl:apply-templates/>
       </fo:block>
     </fo:static-content>
  </xsl:template>

  <xsl:template match="h:title|title">
    <fo:block color="rgb(0,52,120)" font-size="200%" font-weight="bold" margin="0em 0 0 0em">
      <xsl:apply-templates/>
    </fo:block>
    <fo:block color="rgb(68,105,125)" font-size="160%" margin="1em 0 2em 0em">
     <xsl:value-of select="//h:meta[@name='Subtitle']/@content"/>
    </fo:block>
    <fo:block color="rgb(68,105,125)" font-size="140%">
     <xsl:value-of select="//h:meta[@name='Author']/@content"/>
    </fo:block>
    <fo:block color="rgb(68,105,125)" font-size="140%">
     <xsl:value-of select="java:format(java:java.text.SimpleDateFormat.new('yyyy-MM-d'), java:java.util.Date.new())"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="h:body|body">
    <xsl:call-template name="common-atts"/>
    <xsl:apply-templates select="//basefont[1]"/>
    <xsl:apply-templates select="h:h1|h:h2"/>
  </xsl:template>

  <xsl:template match="h:h2">
    <fo:page-sequence master-reference="page">
      <xsl:call-template name="header"/>
      <xsl:call-template name="footer"/>
      <xsl:variable name="slide">
        <!-- group in a div to pass all tags together to overlay.max -->
        <h:div>
          <xsl:apply-templates select="following-sibling::*[1]" mode="overlay.cutslide"/>
        </h:div>
      </xsl:variable>
      <xsl:variable name="snslide" select="common:node-set($slide)"/>
      <xsl:variable name="slidecount">
        <xsl:apply-templates select="$snslide" mode="overlay.max"/>
      </xsl:variable>
      <!-- xsl:message>* slide count for <xsl:value-of select="."/> is <xsl:value-of select="number($slidecount)"/></xsl:message-->
      <fo:flow flow-name="xsl-region-body">
        <xsl:call-template name="overlay.repeat">
          <xsl:with-param name="index" select="1"/>
          <xsl:with-param name="count" select="$slidecount"/>
          <xsl:with-param name="nodeset" select="$snslide"/>
        </xsl:call-template>
        <xsl:if test="count(following-sibling::h:h2)=0">
          <fo:block id="__END__"/>
        </xsl:if>
      </fo:flow>
    </fo:page-sequence>
  </xsl:template>

  <xsl:template name="overlay.repeat">
    <xsl:param name="index"/>
    <xsl:param name="count"/>
    <xsl:param name="nodeset"/>
    <xsl:if test="$index &lt;= $count">
      <!-- xsl:message>Slide instance <xsl:value-of select="$index"/> over <xsl:value-of select="$count"/></xsl:message -->
      <xsl:variable name="filtered.slide">
        <xsl:apply-templates select="$nodeset" mode="layout.filter">
          <xsl:with-param name="index" select="$index"/>
        </xsl:apply-templates>
      </xsl:variable>
      <!-- Control generated RTF for debugging -->
      <!-- xsl:copy-of select="common:node-set($filtered.slide)"/ -->
      <!-- A table to center each slides vertically -->
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="proportional-column-width(1)"/>
        <fo:table-body>
          <fo:table-row height="13.55cm">
            <fo:table-cell display-align="center">
              <xsl:apply-templates select="common:node-set($filtered.slide)"/>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
      <xsl:if test="$index &lt; $count">
        <fo:block break-before="page"/>
      </xsl:if>
      <xsl:call-template name="overlay.repeat">
        <xsl:with-param name="index" select="$index + 1"/>
        <xsl:with-param name="count" select="$count"/>
        <xsl:with-param name="nodeset" select="$nodeset"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="node() | @*" mode="copy">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="copy"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node()" mode="overlay.cutslide">
      <xsl:if test="local-name(.) != 'h2' and local-name(.) != 'h1'">
        <xsl:apply-templates mode="copy" select="."/>
        <xsl:apply-templates mode="overlay.cutslide" select="following-sibling::*[1]"/>
      </xsl:if>
  </xsl:template>
 
  <!-- no filtering on attributes -->
  <xsl:template match="@*" mode="layout.filter">    
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="layout.filter"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/ | node()" mode="layout.filter">
    <xsl:param name="index"/>    
    <xsl:variable name="minmax">
      <xsl:call-template name="overlay.minmax"/>
    </xsl:variable>
    <xsl:variable name="min">
      <xsl:value-of select="number(common:node-set($minmax)/min)"/>
    </xsl:variable>
    <xsl:variable name="max">
      <xsl:value-of select="number(common:node-set($minmax)/max)"/>
    </xsl:variable>
    <!-- xsl:message>Filter <xsl:value-of select="name(.)"/>:<xsl:value-of select="."/></xsl:message>
    <xsl:message>min:<xsl:value-of select="$min"/> max:<xsl:value-of select="$max"/> index:<xsl:value-of select="$index"/></xsl:message-->
    <xsl:copy>
      <!--xsl:if test="$index &lt; $min or $index &gt; $max">
        <xsl:attribute name="visibility">hidden</xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node() | @*" mode="layout.filter">
        <xsl:with-param name="index" select="$index"/>
      </xsl:apply-templates-->
      <xsl:if test="$index &gt;= $min and $index &lt;= $max">
        <xsl:apply-templates select="node() | @*" mode="layout.filter">
          <xsl:with-param name="index" select="$index"/>
        </xsl:apply-templates>
      </xsl:if>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="overlay.minmax">
    <xsl:choose>
      <xsl:when test="@class">
        <xsl:for-each select="str:tokenize(@class)">
          <xsl:variable name="keys" select="substring-after(.,'overlay')"/>
          <xsl:variable name="list" select="str:tokenize($keys, '_')"/>
          <xsl:choose>
            <xsl:when test="not(contains($keys, '_'))">
              <min><xsl:value-of select="$keys"/></min>
              <max><xsl:value-of select="$keys"/></max>              
            </xsl:when>
            <xsl:when test="count($list) &gt; 1">
              <min><xsl:value-of select="$list[1]" /></min>
              <max><xsl:value-of select="$list[2]" /></max>
            </xsl:when>
            <xsl:when test="count($list) &gt; 0">
              <min><xsl:value-of select="$list[1]" /></min>
              <max>2147483648</max>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <min>1</min>
        <max>2147483648</max>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="node()" mode="overlay.max">
    <xsl:variable name="frament">
      <xsl:apply-templates mode="overlay.max.string" select="."/>
    </xsl:variable>
    <xsl:for-each select="common:node-set($frament)/*">
      <xsl:sort data-type="number" select="." order="descending"/>
      <xsl:if test="position()=1">
        <xsl:value-of select="."/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="node()" mode="overlay.max.string">
    <xsl:if test="@class">
      <xsl:for-each select="str:tokenize(@class)">
        <xsl:variable name="list" select="str:tokenize(substring-after(.,'overlay'), '_')"/>
        <xsl:choose>
          <xsl:when test="count($list) &gt; 1">
            <e><xsl:value-of select="$list[2]" /></e>
          </xsl:when>
          <xsl:when test="count($list) &gt; 0">
            <e><xsl:value-of select="$list[1]" /></e>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
    </xsl:if>
    <e>1 </e>
    <xsl:apply-templates mode="overlay.max.string"/>
  </xsl:template>
</xsl:stylesheet>
