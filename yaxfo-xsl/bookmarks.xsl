<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Idea from http://www.ibm.com/developerworks/library/x-xslfo2app/ but for-each sucks -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="h:body|body" mode="bookmarks">
    <fo:bookmark-tree>
      <xsl:apply-templates mode="bookmarks" select="//h:h1"/>
    </fo:bookmark-tree>
  </xsl:template>
 
  <xsl:template match="h:h1" mode="bookmarks">
    <fo:bookmark internal-destination="{generate-id()}">
      <fo:bookmark-title>
        <xsl:number level="any" from="h:body" count="h:h1"/>
        <xsl:text>. </xsl:text>
        <xsl:value-of select="."/>
      </fo:bookmark-title>
      <xsl:apply-templates mode="bookmarks" select="following-sibling::h:h2">
        <xsl:with-param name="current-h1" select="generate-id()"/>
      </xsl:apply-templates>
    </fo:bookmark>
  </xsl:template>
 
  <xsl:template match="h:h2" mode="bookmarks">
    <xsl:param name="current-h1"/>
    <xsl:if test="generate-id(preceding-sibling::h:h1[1]) = $current-h1">
      <fo:bookmark internal-destination="{generate-id()}">
        <fo:bookmark-title>
          <xsl:number level="any" from="h:body" count="h:h1"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h1" count="h:h2"/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
        </fo:bookmark-title>
        <xsl:apply-templates mode="bookmarks" select="following-sibling::h:h3">
          <xsl:with-param name="current-h2" select="generate-id()"/>
        </xsl:apply-templates>
      </fo:bookmark>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h3" mode="bookmarks">
    <xsl:param name="current-h2"/>
    <xsl:if test="generate-id(preceding-sibling::h:h2[1]) = $current-h2">
      <fo:bookmark internal-destination="{generate-id()}">
        <fo:bookmark-title>
          <xsl:number level="any" from="h:body" count="h:h1"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h1" count="h:h2"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h2" count="h:h3"/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
        </fo:bookmark-title>
        <xsl:apply-templates mode="bookmarks" select="following-sibling::h:h4">
          <xsl:with-param name="current-h3" select="generate-id()"/>
        </xsl:apply-templates>
      </fo:bookmark>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="h:h4" mode="bookmarks">
    <xsl:param name="current-h3"/>
    <xsl:if test="generate-id(preceding-sibling::h:h3[1]) = $current-h3">
      <fo:bookmark internal-destination="{generate-id()}">
        <fo:bookmark-title>
          <xsl:number level="any" from="h:body" count="h:h1"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h1" count="h:h2"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h2" count="h:h3"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h3" count="h:h4"/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
        </fo:bookmark-title>
        <xsl:apply-templates mode="bookmarks" select="following-sibling::h:h5">
          <xsl:with-param name="current-h4" select="generate-id()"/>
        </xsl:apply-templates>
      </fo:bookmark>
    </xsl:if>
  </xsl:template>

  <xsl:template match="h:h5" mode="bookmarks">
    <xsl:param name="current-h4"/>
    <xsl:if test="generate-id(preceding-sibling::h:h4[1]) = $current-h4">
      <fo:bookmark internal-destination="{generate-id()}">
        <fo:bookmark-title>
          <xsl:number level="any" from="h:body" count="h:h1"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h1" count="h:h2"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h2" count="h:h3"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h3" count="h:h4"/>
          <xsl:text>. </xsl:text>
          <xsl:number level="any" from="h:h4" count="h:h5"/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
        </fo:bookmark-title>
        <xsl:apply-templates mode="bookmarks" select="following-sibling::h:h6">
          <xsl:with-param name="current-h5" select="generate-id()"/>
        </xsl:apply-templates>
      </fo:bookmark>
    </xsl:if>
  </xsl:template>

  <xsl:template match="h:h6" mode="bookmarks">
    <xsl:param name="current-h5"/>
    <xsl:if test="generate-id(preceding-sibling::h:h5[1]) = $current-h5">
      <fo:bookmark internal-destination="{generate-id()}">
        <fo:bookmark-title>
          <xsl:number level="any" from="h:body" count="h:h1"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h1" count="h:h2"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h2" count="h:h3"/>
          <xsl:text>.</xsl:text>
          <xsl:number level="any" from="h:h3" count="h:h4"/>
          <xsl:text>. </xsl:text>
          <xsl:number level="any" from="h:h4" count="h:h5"/>
          <xsl:text>. </xsl:text>
          <xsl:number level="any" from="h:h5" count="h:h6"/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="."/>
        </fo:bookmark-title>
      </fo:bookmark>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
