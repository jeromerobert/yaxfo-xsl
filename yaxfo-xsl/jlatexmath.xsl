<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:jlm="http://forge.scilab.org/p/jlatexmath"
                exclude-result-prefixes="jlm"
                version='1.0'>
  
  <xsl:template match="jlm:latex" xmlns:jlm="http://forge.scilab.org/p/jlatexmath">
    <fo:instream-foreign-object>
      <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates/>
      </xsl:copy>
    </fo:instream-foreign-object>
  </xsl:template>
  
  <xsl:template match="jlm:*" xmlns:jlm="http://forge.scilab.org/p/jlatexmath">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
