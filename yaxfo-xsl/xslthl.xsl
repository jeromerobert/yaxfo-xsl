<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xhl="http://net.sf.xslthl/ConnectorXalan"
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:xslthl="http://xslthl.sf.net"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    extension-element-prefixes="xhl xslthl">
  
  <!-- for Xalan 2.7 -->
  <xalan:component prefix="xhl" functions="highlight">
    <xalan:script lang="javaclass" src="xalan://net.sf.xslthl.ConnectorXalan" />
  </xalan:component>
  
  <xsl:template match="xhtml:pre">
  <fo:block font-family="DejaVu Sans Mono" white-space-collapse="false" white-space-treatment="preserve"
    linefeed-treatment="preserve" border-style="solid" border-width="0.1mm" padding="2mm" font-size="90%"
    margin-top="1.5em" margin-bottom="1.5em">
    <xsl:call-template name="common-atts"/>
    <!-- xsl:apply-templates select="xhl:highlight(@lang, .)" mode="debug"/ -->
    <xsl:apply-templates select="xhl:highlight(@lang, .)" mode="xslthl"/>
  </fo:block>
  </xsl:template>
  
  <xsl:template match="*" mode="debug">
    <xsl:message><xsl:value-of select="name()"/></xsl:message>
  </xsl:template>

  <xsl:template match='xslthl:keyword' mode="xslthl">
    <fo:inline font-weight="bold">
      <xsl:apply-templates mode="xslthl"/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match='xslthl:string' mode="xslthl">
    <fo:inline font-weight="bold" font-style="italic">
      <xsl:apply-templates mode="xslthl"/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match='xslthl:comment' mode="xslthl">
    <fo:inline font-style="italic">
      <xsl:apply-templates mode="xslthl"/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match='xslthl:tag' mode="xslthl">
    <fo:inline font-weight="bold" color="blue"><xsl:apply-templates mode="xslthl"/></fo:inline>
  </xsl:template>
  
  <xsl:template match='xslthl:attribute' mode="xslthl">
    <fo:inline font-weight="bold" color="green">
      <xsl:apply-templates mode="xslthl"/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match='xslthl:value' mode="xslthl">
    <fo:inline font-weight="bold" color="orange">
      <xsl:apply-templates mode="xslthl"/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match='xslthl:number' mode="xslthl">
    <xsl:apply-templates mode="xslthl"/>
  </xsl:template>
  
  <xsl:template match='xslthl:annotation' mode="xslthl">
    <fo:inline color="gray">
      <xsl:apply-templates mode="xslthl"/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match='xslthl:directive' mode="xslthl">
    <xsl:apply-templates mode="xslthl"/>
  </xsl:template>
  
  <!-- Not sure which element will be in final XSLTHL 2.0 -->
  <xsl:template match='xslthl:doccomment|xslthl:doctype' mode="xslthl">
    <fo:inline font-weight="bold">
      <xsl:apply-templates mode="xslthl"/>
    </fo:inline>
  </xsl:template>
  
</xsl:stylesheet>
