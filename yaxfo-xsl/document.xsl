<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xhtml="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="xhtml">
 
  <xsl:include href="xhtml2fo.xsl"/>
  <xsl:include href="toc.xsl"/>
  <xsl:include href="table.xsl"/>
  <xsl:include href="svg.xsl"/>
  <xsl:include href="jlatexmath.xsl"/>
  <xsl:include href="mathml.xsl"/>
  <xsl:include href="bookmarks.xsl"/>
  <xsl:include href="title1.xsl"/>
  <xsl:include href="xslthl.xsl"/>
  <xsl:param name="font-size" select="'11pt'"/>
  <xsl:param name="font.symbol" select="'any'"/>

  <xsl:template match="xhtml:html|html">
    <fo:root font-family="DejaVu Sans Condensed">
      <fo:layout-master-set>
        <fo:simple-page-master master-name="page" page-width="21cm" page-height="29.7cm">
          <fo:region-body margin="2cm 2cm 2cm 2cm"/>
          <fo:region-after extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <xsl:apply-templates mode="bookmarks"/>
      <fo:page-sequence master-reference="page">
        <fo:static-content flow-name="xsl-region-after">
          <fo:block display-align="after" text-align="center" font-size="8pt" margin-bottom="1em">
            <fo:page-number/>
            <xsl:text> / </xsl:text>
            <fo:page-number-citation ref-id="__END__"/>
          </fo:block>
          <xsl:call-template name="footer"/>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <xsl:if test="$font-size">
            <xsl:attribute name="font-size">
              <xsl:value-of select="$font-size"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:call-template name="title-page"/>
          <fo:block break-after='page'/>
          <fo:block margin-bottom="1em" text-align="center" font-size="160%" font-weight="bold">Table of content</fo:block>
          <xsl:apply-templates mode="toc"/>
          <fo:block break-after='page'/>
          <xsl:apply-templates/>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
 
  <xsl:template name="centered-title">
    <fo:table table-layout="fixed" width="100%">
      <fo:table-column column-width="proportional-column-width(1)"/>
      <fo:table-body>
        <fo:table-row height="25.7cm">
          <fo:table-cell display-align="center">
            <fo:block text-align="center" font-size="200%" font-weight="bold">
              <xsl:apply-templates/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>
 
  <xsl:template match="xhtml:body|body">
    <xsl:call-template name="common-atts"/>
    <xsl:apply-templates select="//basefont[1]"/>
    <xsl:apply-templates/>
    <fo:block id="__END__"/>
  </xsl:template>

  <xsl:template match="xhtml:a[@href]|a[@href]">
    <xsl:if test="@type">
      <xsl:attribute name="content-type">
        <xsl:value-of select="@type"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:choose>
      <xsl:when test="starts-with(@href,'#')">
        <xsl:variable name="anchor" select="substring-after(@href,'#')"/> 
        <xsl:variable name="target" select="//xhtml:a[@name=$anchor]/.."/> 
        <fo:basic-link internal-destination="{$anchor}">
          <xsl:apply-templates/>
          (§ <xsl:apply-templates mode="section_id" select="$target"/>
          page <fo:page-number-citation ref-id="{generate-id($target)}"/>)
        </fo:basic-link>
      </xsl:when>
      <xsl:otherwise>
        <fo:basic-link color="blue" text-decoration="underline">
          <xsl:attribute name="external-destination">
            <xsl:text>url(&apos;</xsl:text>
            <xsl:value-of select="concat(//base/@href,@href)"/>
            <xsl:text>&apos;)</xsl:text>
          </xsl:attribute>
          <xsl:call-template name="common-atts"/>
          <xsl:apply-templates/>
        </fo:basic-link>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- to be overidden by custom xsl -->
  <xsl:template name="footer"/>
</xsl:stylesheet>
