<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
  <xsl:import href="urn:publicid:yaxfo-xsl:document"/>

  <xsl:template name="footer">
    <fo:block display-align="after" text-align="center" font-size="8pt">
      <fo:external-graphic src="url('http://i.creativecommons.org/l/by-nc/3.0/80x15.png')" content-height="1em"/>
      This work is licensed under a
      <fo:basic-link color="blue" text-decoration="underline" external-destination="url('http://creativecommons.org/licenses/by-nc/3.0/')">
        Creative Commons Attribution-NonCommercial 3.0 Unported License.
      </fo:basic-link>
    </fo:block>
  </xsl:template>

  <xsl:template name="title-page">
    <xsl:call-template name="title-head-table"/>
    <xsl:call-template name="title-center-table"/>
    <xsl:call-template name="title-bottom-table"/>
  </xsl:template>

  <xsl:template name="title-bottom-table">
    <fo:table width="100%" table-layout="fixed" border-style="solid" border-width="0.5mm" font-weight="bold">
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.5mm" text-align="center" display-align="center">
            <fo:block>Responsability</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.5mm" text-align="center" display-align="center">
            <fo:block>Name</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.5mm" text-align="center" display-align="center" padding="0.2em 0 0.2em 0">
            <fo:block>Service</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.5mm" text-align="center" display-align="center">
            <fo:block>Date</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.5mm" text-align="center" display-align="center">
            <fo:block>Visa</fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.5mm" display-align="center" padding="0.8em 0.5em 0.8em 0.5em">
            <fo:block>Author(s)</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" display-align="center" text-align="center">
            <fo:block>
              <xsl:value-of select="//h:meta[@name='Author']/@content"/>
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.5mm" display-align="center" padding="0.8em 0.5em 0.8em 0.5em">
            <fo:block>Team leader</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.5mm" display-align="center" padding="0.8em 0.5em 0.8em 0.5em">
            <fo:block>Head of department</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
          <fo:table-cell border-style="solid"> <fo:block/> </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template name="title-center-table">
    <fo:table table-layout="fixed" font-size="140%" font-weight="bold"
      border-style="solid" border-width="0.5mm" margin="2cm 3cm 2cm 3cm">
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell padding="1em 1em 1em 1em">
            <fo:block text-align="center">
              <xsl:value-of select="//h:title"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template name="title-head-table">
    <xsl:param name="company" select="'&lt;company&gt;'"/>
    <xsl:param name="department" select="'&lt;department&gt;'"/>
    <xsl:param name="domain" select="'&lt;domain&gt;'"/>
    <xsl:param name="contract" select="'&lt;contract&gt;'"/>
    <fo:table width="100%" table-layout="fixed" border-style="solid" border-width="0.5mm">
      <fo:table-column column-width="23%"/>
      <fo:table-column column-width="9%"/>
      <fo:table-column column-width="16%"/>
      <fo:table-column column-width="18%"/>
      <fo:table-column column-width="21%"/>
      <fo:table-column column-width="13%"/>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell number-rows-spanned="2" border-style="solid" border-width="0.2mm">
            <fo:block/>
          </fo:table-cell>
          <fo:table-cell display-align="center" number-columns-spanned="3" number-rows-spanned="2">
            <fo:block font-weight="bold" font-size="109%" text-align="center">TECHNICAL NOTE</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="2" number-rows-spanned="1" border-style="solid" border-width="0.2mm" padding="0.2em">
            <fo:block text-align="center" color="#0000ff" font-weight="bold">
              <xsl:value-of select="$company"/>
            </fo:block>
            <fo:block space-before=".6em" text-align="center" font-weight="bold" font-size="109%">
              <xsl:value-of select="$department"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.2mm" padding="0.2em">
            <fo:block text-align="center" font-size="82%">
              <xsl:value-of select="java:format(java:java.text.SimpleDateFormat.new('yyyy-MM-d'), java:java.util.Date.new())"/>
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" padding="0.2em">
            <fo:block text-align="center" font-size="82%">
              <fo:page-number/>
              / <fo:page-number-citation ref-id="__END__"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.2mm">
            <fo:block font-weight="bold" text-align="center">N° contract or Study Ref.</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" number-columns-spanned="2" display-align="center">
            <fo:block text-align="center" font-weight="bold">Customer Organisation</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" display-align="center">
            <fo:block font-weight="bold" text-align="center">Confidentiality</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" number-columns-spanned="2" display-align="center">
            <fo:block font-weight="bold" text-align="center">
              <xsl:value-of select="$contract"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.2mm">
            <fo:block text-align="center">
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" number-columns-spanned="2">
            <fo:block text-align="center">
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm">
            <fo:block text-align="center">
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" number-columns-spanned="2" padding="0.1em">
            <fo:block text-align="center" font-weight="bold">
              <xsl:value-of select="$domain"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" border-width="0.2mm" number-columns-spanned="2" padding="0.1em">
            <fo:block text-align="center" font-weight="bold" font-size="91%">Reference</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" padding="0.1em">
            <fo:block text-align="center" font-weight="bold" font-size="82%">Version/index</fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" number-columns-spanned="3" padding="0.1em">
            <fo:block text-align="center" font-weight="bold" font-size="82%">
Cancels and replaces the previous document (Ref. &amp; Version):
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell border-style="solid" height="2em" border-width="0.2mm" number-columns-spanned="2">
            <fo:block text-align="center">
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm">
            <fo:block text-align="center">
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="0.2mm" number-columns-spanned="3">
            <fo:block text-align="center">
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>
</xsl:stylesheet>

