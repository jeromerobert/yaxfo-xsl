<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Customized from http://tecfa.unige.ch/guides/xml/examples/xsl-toc/xhtml-toc-ns-numbered.xsl -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="h:body|body" mode="toc">
    <fo:list-block provisional-label-separation=".2em" provisional-distance-between-starts="1.6em">
      <xsl:apply-templates mode="toc" select="h:h1"/>
    </fo:list-block>
  </xsl:template>
 
  <xsl:template match="h:h1" mode="toc">
    <fo:list-item margin-top="0.5em" keep-together.within-page="always">
      <fo:list-item-label end-indent="label-end()">
        <fo:block text-align="end">
          <xsl:text disable-output-escaping="yes">&amp;#x2022;</xsl:text>
        </fo:block>
      </fo:list-item-label>
      <fo:list-item-body start-indent="body-start()">
        <fo:block>
          <fo:basic-link internal-destination="{generate-id(.)}">
            <xsl:value-of select="."/>
          </fo:basic-link>
    	  <fo:list-block provisional-label-separation=".2em" provisional-distance-between-starts="1.6em">
            <xsl:apply-templates select="following-sibling::*[1]" mode="toc2"/>
          </fo:list-block>
        </fo:block>
      </fo:list-item-body>
    </fo:list-item>
  </xsl:template>
 
  <xsl:template match="*" mode="toc2">
    <xsl:if test="local-name(.)='h2'">
      <fo:list-item>
        <fo:list-item-label end-indent="label-end()">
          <fo:block text-align="end">
            <xsl:text disable-output-escaping="yes">&amp;#x2022;</xsl:text>
          </fo:block>
        </fo:list-item-label>
        <fo:list-item-body start-indent="body-start()">
          <fo:block>
            <fo:basic-link internal-destination="{generate-id(.)}">
              <xsl:value-of select="."/>
            </fo:basic-link>
          </fo:block>
        </fo:list-item-body>
      </fo:list-item>
    </xsl:if>
    <xsl:if test="local-name(.)!='h1'">
      <xsl:apply-templates select="following-sibling::*[1]" mode="toc2"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
