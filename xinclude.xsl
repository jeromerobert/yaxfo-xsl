<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" 
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xi="http://www.w3.org/2001/XInclude"
   xmlns:xi2="http://www.w3.org/2003/XInclude"
   version="1.0">
 
  <xsl:template match="xi:include">
    <xsl:apply-templates select="document(@href)/*"/>
  </xsl:template>
 
  <xsl:template match="xi2:include">
    <xsl:apply-templates select="document(@href)/*"/>
  </xsl:template>
 
</xsl:stylesheet>
