#! /usr/bin/make -f

TOP := $(dir $(lastword $(MAKEFILE_LIST)))
#export JAVA_CLASSPATH=$(TOP)
DATE=$(shell date +%Y-%m-%d)
FOP=/usr/bin/fop
#FOP=/home/jerome/fop/fop

%.fo : %.html $(XSL)
	$(FOP) -catalog -xml $< -xsl $(XSL) -foout $@
	xmlindent -i2 -w $@
	rm $*.fo~

%.fo : %.xml $(XSL)
	$(FOP) -catalog -xml $< -xsl $(XSL) -foout $@
	xmlindent -i2 -w $@
	rm $*.fo~

%.awt : %.fo
	$(FOP) -fo $< -awt

%.pdf : %.fo
	$(FOP) -fo $< -pdf $@

%.clean:
	rm $*.fo $*.pdf

.PHONY: %.awt %.clean
.SECONDARY:

