#! /usr/bin/python
from lxml.etree import *
import copy
import math
import sys

def overlay_interval(element):
    clazz = element.get("class")
    if clazz:
        for c in clazz.split(" "):
            if c.startswith("overlay"):
                l = c[7:].split("_")
                n = len(l)
                if n == 1:
                    return (int(l[0]), int(l[0]))
                elif n == 2:
                    if len(l[1]) == 0:
                        return (int(l[0]), -1)
                    else:
                        return (int(l[0]), int(l[1]))
    return (-1, -1)

def last_overlay(elements):
    r = -1
    for e in elements:
        r = max(r, last_overlay(e.getchildren()), overlay_interval(e)[1])
    if r < 0:
        return 1
    else:
        return r

def process_slide(body, istart, iend, islide):
    elements = body.getchildren()[istart + 1: iend]
    nb_overlay = last_overlay(elements)
    body.insert(istart + 1, copy.deepcopy(elements[0]))
    print elements[0]
    print nb_overlay

doc = parse(sys.argv[1])
ns="{http://www.w3.org/1999/xhtml}"
body = doc.find("%sbody" % ns)
children = body.getchildren()
nchildren = len(children)
istart=-1
islide=1

for i in xrange(nchildren):
    e = children[i]
    if e.tag == "%sh1" % ns:
        if istart >= 0:
            process_slide(body, istart, i, islide)
            islide = islide + 1
        istart = i
if istart >= 0:
    process_slide(body, istart, nchildren, islide + 1)

doc.write(sys.argv[2])
