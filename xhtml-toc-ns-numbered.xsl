<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns="http://www.w3.org/1999/xhtml">

  <!-- xhtml-toc.xsl Stylesheet to create a TOC client-side for h1 and h2 XHTML elements.
       It also will number h1 and h2's.
       Daniel Schneider from tecfa.unig.ch (feb 2007)
       Get it from: http://tecfa.unige.ch/guides/xml/examples/xsl-toc/

       Tested with Firefox 1.5x (linux) and 2.0x (win). Does not work with IE 6.
       If you change encoding you have to fix this file, i.e replace the &#160 and � characters
       XHTML should be real XML like below and h1 and h2 headers can be any place I think
       - - - - -
      <?xml version="1.0" encoding="ISO-8859-1" ?>
      <?xml-stylesheet href="xhtml-toc.xsl" version="1.0" type="text/xsl"?>
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                             "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

        <html xmlns:h="http://www.w3.org/1999/xhtml">
          <head> ... </head><body> ... </body> ... </html>
       - - - - -
       Notes: Carefully respect the namespaces !!
       Thanx to Martin Honnen and David Carlisle (XSL mailing list)          
       -->    

  <xsl:output
    method="xml"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    indent="yes"
    encoding="iso-8859-1"
    />
<!-- kill on May 2009
    exclude-result-prefixes="h" -->

  <xsl:template match="/h:html">
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <title><xsl:value-of select="h:title"/></title>
    </head>
    <body bgcolor="#FFFFFF">
      <xsl:apply-templates select="h:body"/>
    </body>
  </html>
</xsl:template>

<xsl:template match="h:body">
  <strong><a name="toc">Contents</a></strong>
  <xsl:apply-templates select="h:h1|h:h2" mode="toc"/>
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="h:h1" mode="toc">
  <br/>
  <xsl:number level="any" from="h:body" count="h:h1"/>
  <xsl:text>. </xsl:text>
  <a href="#h1_{generate-id(.)}"><xsl:value-of select="."/></a><br/>
</xsl:template>

<xsl:template match="h:h2" mode="toc">
  <span style="font-size:small;">
    <xsl:text> &#160;&#160;� </xsl:text>
    <xsl:number level="any" from="h:body" count="h:h1"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h1" count="h:h2"/>
    <xsl:text> </xsl:text>
    <a href="#h2_{generate-id(.)}"><xsl:value-of select="."/></a>
  </span>
</xsl:template>

<xsl:template match="h:h1">
  <h1><a name="h1_{generate-id(.)}">
  <xsl:number level="any" from="h:body" count="h:h1"/>
  <xsl:text>. </xsl:text>
  <xsl:value-of select="."/></a> (<a href="#toc">&#171;up</a>)</h1>
</xsl:template>

<xsl:template match="h:h2">
  <h2><a name="h2_{generate-id(.)}">
    <xsl:number level="any" from="h:body" count="h:h1"/>
    <xsl:text>.</xsl:text>
    <xsl:number level="any" from="h:h1" count="h:h2"/>
    <xsl:text> </xsl:text>
  <xsl:value-of select="."/></a> (<a href="#toc">&#171;up</a>)</h2>
</xsl:template>


<!-- A default rule will just copy all the rest -->

<xsl:template match="*">
     <xsl:copy>
         <xsl:copy-of select="@*"/>
       <xsl:apply-templates/>
     </xsl:copy>
   </xsl:template>

</xsl:stylesheet>
